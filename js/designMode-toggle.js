(function() {
	window.addEventListener('keydown', function(e){
		if (e.key == 'Escape' || e.key == 'Esc' || e.keyCode == 27) {
			document.designMode = 'off';
		}
	});
	if (document.designMode === 'off') {
		document.designMode = 'on';
	} else if (document.designMode === 'on') {
		document.designMode = 'off';
	}
})();