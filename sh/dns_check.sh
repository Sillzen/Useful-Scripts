#!/bin/bash

echo -n "Enter URL: "
read url
echo -n "Expected value: "
read expected
echo -n "Refresh time (s): "
read sleeptime

while true
do
	echo
	echo "Lookup results for $url on `date`"
	echo
	nslookup $url | tail -3
	nslookup $url | grep -q "$expected"
	if [ $? == 0 ]; then
		echo "Expected value: $expected"
		echo "Actual value: `nslookup $url | tail -2 | sed 's/.*://'`"
		echo "DNS match!"
	elif [ $? > 0 ]; then
		echo "Expected value: $expected"
		echo "Actual value: `nslookup $url | tail -2 | sed 's/.*://'`"
		echo "DNS does not match."
	fi
	echo
	echo "======================"
	sleep $sleeptime
done