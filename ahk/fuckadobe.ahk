; Script for setting MMB to spacebar because Adobe overlords decided that MMB pan is useless and the user shouldn't be allowed to set it

; +Escape::
; ExitApp
; Return

; ^ Exit Script

#IfWinActive ahk_class Photoshop

  MButton::

    Send {Space Down}{LButton Down}
    Keywait, MButton
    Send {Space Up}{LButton Up}

  Return
  
#IfWinActive

#IfWinActive ahk_class illustrator

  MButton::

    Send {Space Down}{LButton Down}
    Keywait, MButton
    Send {Space Up}{LButton Up}

  Return
  
#IfWinActive