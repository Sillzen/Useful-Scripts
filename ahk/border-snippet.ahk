; ctrl + b outputs "border: 1px solid [colour];"

setworkingdir %a_scriptdir%

fileread, file, borders.txt
; | is the separator - can be anything
stringsplit, text, file, |

^b::
 {
   Random, ran, 1, %text0%
   sendinput, % text%ran%
 }
return