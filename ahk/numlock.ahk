; When numlock is off, set the number keys to do other things rather than the alt functions (home, arrow keys, pg up pg dn etc.)
; https://autohotkey.com/docs/commands/GetKeyState.htm
; https://autohotkey.com/boards/viewtopic.php?t=7009
; ctrl f numpad https://autohotkey.com/docs/KeyList.htm
; 7 8 9
; 4 5 6
; 1 2 3
; ->
; NumpadHome NumpadUp NumpadPgUp
; NumpadLeft NumpadClear NumpadRight
; NumpadEnd NumpadDown NumpadPgDn