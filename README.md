# ahk
Requires [AutoHotKey](https://autohotkey.com/download/) (Windows only) to be installed to use. Alternatively, these can be packaged as .exe files using AHK and then distributed for standalone use.

## border-snippet.ahk
Ctrl + B for a random `border: 1px solid {colour};`, pulled from `borders.txt`.

## fuckadobe.ahk
MMB == space when Photoshop or Illustrator is the active window. This allows you to pan while holding down MMB. Newer versions of Photoshop use some sort of smooth-scrolling thing when panning, this can be turned off in Prefs -> General Options -> Tools -> "Enable Flick Panning"

## numlock.ahk
todo.

## window_on_top.ahk
Pressing Ctrl + Space in the active window keeps that window on top of other windows. Toggled off by pressing Ctrl + Space again in that window.

## window_size_cycle.ahk
Cycles size of active window by pressing Left Alt + [1-6]. Provides a tooltip showing you current size of the window after changing.

# CSS

## Unset
`* { all: unset !important; }` Forces all styles to be [unset](https://developer.mozilla.org/en/docs/Web/CSS/unset), defaulting them to the `initial` value. Works in majority of browsers, except IE.

# Javascript

## designMode
Turns [`designMode`](https://developer.mozilla.org/en-US/docs/Web/API/Document/designMode) on or off for the current page. Easiest way to use this is to make a bookmarklet with `javascript:` before the code, and add it to the bookmarks bar of your browser and click to use.

## elementBorders
Adds a randomly coloured border to each element on the page. Best used as a bookmarklet. [Credit](https://www.reddit.com/r/webdev/comments/a3dxtf/best_css_tools/eb7clio/).

## executionTime
Tells you how long it takes to execute whatever you put between `console.time()` and `console.timeEnd()`.

# Shell

## dns_check.sh
Enter a URL, an expected value for the A record and time between checks. Outputs the name + A record along with a message if it does/does not match.